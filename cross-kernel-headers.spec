
%define cross cross
%define rpmprefix %{nil}

# These build_* macros match cross-binutils.spec.
#
# The subpackages created here are only really useful if the "sysroot"
# directory paths where they install the kernel headers match the
# sysroot that the cross-tools are looking for.  So be sure to keep
# this list and the "do_package", "install_package", and "do_files"
# lists of target names in synch with cross-{binutils,gcc}.spec.

%define build_all		1
%define build_alpha		%{build_all}
%define build_arm		%{build_all}
%define build_aarch64		%{build_all}
%define build_avr32		%{build_all}
%define build_blackfin		%{build_all}
%define build_c6x		%{build_all}
%define build_frv		%{build_all}
%define build_h8300		%{build_all}
%define build_hppa		%{build_all}
%define build_hppa64		%{build_all}
%define build_ia64		%{build_all}
%define build_m32r		%{build_all}
%define build_m68k		%{build_all}
%define build_metag		%{build_all}
%define build_microblaze	%{build_all}
%define build_mips64		%{build_all}
%define build_mn10300		%{build_all}
%define build_openrisc		%{build_all}
%define build_powerpc64		%{build_all}
%define build_s390x		%{build_all}
%define build_score		%{build_all}
%define build_sh		%{build_all}
%define build_sh64		%{build_all}
%define build_sparc64		%{build_all}
%define build_tile		%{build_all}
%define build_x86_64		%{build_all}
%define build_xtensa		%{build_all}

# As of linux-3.15, 'make ARCH=cris headers_install' fails out of the box.
%define build_cris		0

# 32-bit packages we don't build as we can use the 64-bit package instead
%define build_i386		0
%define build_mips		0
%define build_powerpc		0
%define build_s390		0
%define build_sparc		0
%define build_sh4		0

# not available in binutils-2.24
%define build_hexagon		0
%define build_unicore32		0


Summary: Header files for the Linux kernel for use by glibc
Name: %{cross}-kernel-headers
Version: 3.15
Release: 1%{?dist}
License: GPLv2
Group: Development/System
URL: http://www.kernel.org/
BuildArch: noarch

Source: ftp://ftp.kernel.org/pub/linux/kernel/v3.0/linux-%{version}.tar.xz

%description
Kernel-headers includes the C header files that specify the interface
between the Linux kernel and userspace libraries and programs.  The
header files define structures and constants that are needed for
building most standard programs and are also needed for rebuilding the
glibc package.

%define do_package() \
%if %2 \
%package -n %{rpmprefix}kernel-headers-%1 \
Summary: Cross-build Linux kernel headers for %1 \
Group: Development/System \
%description -n %{rpmprefix}kernel-headers-%1 \
Linux kernel header files for cross-compilation support. \
%endif


# The "do_symlink" (and "symlink_target", below) cases are where there
# is more than one toolchain target with a distinct GNU tuple and
# tooldir for it, corresponding to a single kernel arch/ subdirectory.
# For each of these sets, one target tuple is chosen (arbitrarily) as
# "primary" and the other packages just depend on the primary target's
# package and populate their own sysroot's /usr/include with symlinks
# into the primary target's sysroot.
#
# Note that this is a completely disjoint set of cases from the ones
# that are "do_symlink" in cross-binutils.spec: those don't actually
# have separate toolchains and just symlink the entire tooldir, so
# there is no need for another kernel-headers package at all.

%define do_symlink() \
%if %2 \
%package -n %{rpmprefix}kernel-headers-%1 \
Summary: Cross-build Linux kernel headers for %1 \
Group: Development/System \
Requires: %{rpmprefix}kernel-headers-%3 == %{version}-%{release} \
%description -n %{rpmprefix}kernel-headers-%1 \
Linux kernel header files for cross-compilation support. \
%endif

%do_package alpha-linux-gnu		%{build_alpha}
%do_package arm-linux-gnu		%{build_arm}
%do_package aarch64-linux-gnu		%{build_aarch64}
%do_package avr32-linux-gnu		%{build_avr32}
%do_package bfin-linux-gnu		%{build_blackfin}
%do_package c6x-linux-gnu		%{build_c6x}
%do_package cris-linux-gnu		%{build_cris}
%do_package frv-linux-gnu		%{build_frv}
%do_package hexagon-linux-gnu		%{build_hexagon}
%do_package hppa-linux-gnu		%{build_hppa}
%do_symlink hppa64-linux-gnu		%{build_hppa64}		hppa-linux-gnu
%do_symlink i386-linux-gnu		%{build_i386}		x86_64-linux-gnu
%do_package ia64-linux-gnu		%{build_ia64}
%do_package m32r-linux-gnu		%{build_m32r}
%do_package m68k-linux-gnu		%{build_m68k}
%do_package metag-linux-gnu		%{build_metag}
%do_package microblaze-linux-gnu	%{build_microblaze}
%do_package mips64-linux-gnu		%{build_mips64}
%do_package mips-linux-gnu		%{build_mips}		mips64-linux-gnu
%do_package mn10300-linux-gnu		%{build_mn10300}
%do_package openrisc-linux-gnu		%{build_openrisc}
%do_package powerpc64-linux-gnu		%{build_powerpc64}
%do_symlink powerpc-linux-gnu		%{build_powerpc}	powerpc64-linux-gnu
%do_package s390x-linux-gnu		%{build_s390x}
%do_symlink s390-linux-gnu		%{build_s390}		s390x-linux-gnu
%do_package score-linux-gnu		%{build_score}
%do_package sh-linux-gnu		%{build_sh}
%do_symlink sh4-linux-gnu		%{build_sh4}		sh-linux-gnu
%do_symlink sh64-linux-gnu		%{build_sh64}		sh-linux-gnu
%do_symlink sparc-linux-gnu		%{build_sparc}		sparc64-linux-gnu
%do_package sparc64-linux-gnu		%{build_sparc64}
%do_package tile-linux-gnu		%{build_tile}
%do_package unicore32-linux-gnu		%{build_unicore32}
%do_package x86_64-linux-gnu		%{build_x86_64}
%do_package xtensa-linux-gnu		%{build_xtensa}


%prep
%setup -q -n linux-%{version}

%build

%install
rm -rf %{buildroot}

# The first argument "target" is the GNU tool target tuple.
# The third argument "arch" is the kernel ARCH= architecture name.
install_target() {
  local target="$1"
  local cond="$2"
  local arch="$3"

  if [ $cond -ne 0 ]; then
    echo "=== INSTALL target $target ==="
    local sysroot="%{buildroot}%{_prefix}/${target}/sys-root"
    mkdir -p "$sysroot"
    # Note that this will complain about not finding the cross-compiler
    # because we don't have a BuildRequires for it.  But not having the
    # compiler doesn't actually make a difference in the installed headers,
    # so we don't bloat the BuildRequires just to tidy the build log.
    # It would be "BuildRequires: %{rpmprefix}gcc-%1 \" after %%package
    # in the %%do_package macro.
    make %{_smp_mflags} \
	 ARCH="${arch}" \
	 CROSS_COMPILE="${target}-" \
	 INSTALL_HDR_PATH="${sysroot}/usr" \
	 headers_install
    find "${sysroot}/usr" -name '.*' ! -type d -print0 | xargs -0 rm -f
  fi
}

# The first argument "target" is the target tuple being created.
# The third argument "other" is the other target that will symlink to.
# This expects that "install_target $target" has already run.
symlink_target() {
  local target="$1"
  local cond="$2"
  local other="$3"

  if [ $cond -ne 0 ]; then
    echo "=== SYMLINK target $target -> $other ==="
    local sysroot="%{buildroot}%{_prefix}/${target}/sys-root"
    local includedir="${sysroot}/usr/include"
    mkdir -p "${includedir}"
    (
      cd "${includedir}"
      ln -snf ../../../../${other}/sys-root/usr/include/* .
    )
  fi
}

install_target alpha-linux-gnu		%{build_alpha}		alpha
install_target arm-linux-gnu		%{build_arm}		arm
install_target aarch64-linux-gnu	%{build_aarch64}	arm64
install_target avr32-linux-gnu		%{build_avr32}		avr32
install_target bfin-linux-gnu		%{build_blackfin}	blackfin
install_target c6x-linux-gnu		%{build_c6x}		c6x
install_target cris-linux-gnu		%{build_cris}		cris
install_target frv-linux-gnu		%{build_frv}		frv
install_target hexagon-linux-gnu	%{build_hexagon}	hexagon
install_target hppa-linux-gnu		%{build_hppa}		parisc
symlink_target hppa64-linux-gnu		%{build_hppa64}		hppa-linux-gnu
symlink_target i386-linux-gnu		%{build_i386}		x86_64-linux-gnu
install_target ia64-linux-gnu		%{build_ia64}		ia64
install_target m32r-linux-gnu		%{build_m32r}		m32r
install_target m68k-linux-gnu		%{build_m68k}		m68k
install_target metag-linux-gnu		%{build_metag}		metag
install_target microblaze-linux-gnu 	%{build_microblaze}	microblaze
install_target mips64-linux-gnu		%{build_mips64}		mips
symlink_target mips-linux-gnu		%{build_mips}		mips64-linux-gnu
install_target mn10300-linux-gnu	%{build_mn10300}	mn10300
install_target openrisc-linux-gnu	%{build_openrisc}	openrisc
install_target powerpc64-linux-gnu	%{build_powerpc64}	powerpc
symlink_target powerpc-linux-gnu	%{build_powerpc}	powerpc64-linux-gnu
install_target s390x-linux-gnu		%{build_s390x}		s390
symlink_target s390-linux-gnu		%{build_s390}		s390x-linux-gnu
install_target score-linux-gnu		%{build_score}		score
install_target sh-linux-gnu		%{build_sh}		sh
symlink_target sh4-linux-gnu		%{build_sh4}		sh-linux-gnu
symlink_target sh64-linux-gnu		%{build_sh64}		sh-linux-gnu
install_target sparc64-linux-gnu	%{build_sparc64}	sparc
symlink_target sparc-linux-gnu		%{build_sparc}		sparc64-linux-gnu
install_target tile-linux-gnu		%{build_tile}		tile
install_target unicore32-linux-gnu	%{build_unicore32}	unicore32
install_target x86_64-linux-gnu		%{build_x86_64}		x86
install_target xtensa-linux-gnu		%{build_xtensa}		xtensa


%define do_files() \
%if %2 \
%files -n %{rpmprefix}kernel-headers-%1 \
%defattr(-,root,root) \
%{_prefix}/%1/sys-root/usr/include/* \
%endif

%do_files alpha-linux-gnu	%{build_alpha}
%do_files arm-linux-gnu		%{build_arm}
%do_files aarch64-linux-gnu	%{build_aarch64}
%do_files avr32-linux-gnu	%{build_avr32}
%do_files bfin-linux-gnu	%{build_blackfin}
%do_files c6x-linux-gnu		%{build_c6x}
%do_files cris-linux-gnu	%{build_cris}
%do_files frv-linux-gnu		%{build_frv}
%do_files hexagon-linux-gnu	%{build_hexagon}
%do_files hppa64-linux-gnu	%{build_hppa64}
%do_files hppa-linux-gnu	%{build_hppa}
%do_files i386-linux-gnu	%{build_i386}
%do_files ia64-linux-gnu	%{build_ia64}
%do_files m32r-linux-gnu	%{build_m32r}
%do_files m68k-linux-gnu	%{build_m68k}
%do_files metag-linux-gnu	%{build_metag}
%do_files microblaze-linux-gnu	%{build_microblaze}
%do_files mips-linux-gnu	%{build_mips}
%do_files mips64-linux-gnu	%{build_mips64}
%do_files mn10300-linux-gnu	%{build_mn10300}
%do_files openrisc-linux-gnu	%{build_openrisc}
%do_files powerpc-linux-gnu	%{build_powerpc}
%do_files powerpc64-linux-gnu	%{build_powerpc64}
%do_files s390-linux-gnu	%{build_s390}
%do_files s390x-linux-gnu	%{build_s390x}
%do_files score-linux-gnu	%{build_score}
%do_files sh-linux-gnu		%{build_sh}
%do_files sh4-linux-gnu		%{build_sh4}
%do_files sh64-linux-gnu	%{build_sh64}
%do_files sparc-linux-gnu	%{build_sparc}
%do_files sparc64-linux-gnu	%{build_sparc64}
%do_files tile-linux-gnu	%{build_tile}
%do_files unicore32-linux-gnu	%{build_unicore32}
%do_files x86_64-linux-gnu	%{build_x86_64}
%do_files xtensa-linux-gnu	%{build_xtensa}


%changelog
* Thu Jul  3 2014 Roland McGrath <roland@hack.frob.com>
- Initial version.
